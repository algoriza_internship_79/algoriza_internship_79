import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import Login from '../views/Login.vue'
import Register from '../views/Register.vue'
// import Res from '../components/Res.vue'
import MyTrips from '../views/MyTrips.vue'
import SearchResult from '../views/SearchResult.vue'
import ProductDetails from '../views/ProductDetails.vue'
import checkOut from '../views/checkOut.vue'
import LogedHome from '../views/LogedHome.vue'
const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/Register',
    name: 'Register',
    component: Register
  },
  {
    path: '/Login',
    name: 'Login',
    component: Login
  },
  {
    path:'/SearchResult',
    name:'SearchResult',
    component:SearchResult,
    props:true
  },
  {
    path:'/MyTrips',
    name:'MyTrips',
    component:MyTrips

  },
  {
    path:'/ProductDetails',
    name:'ProductDetails',
    component:ProductDetails

  },
  {
    path:'/checkOut',
    name:'checkOut',
    component:checkOut

  },
  {
    path:'/LogedHome',
    name:'LogedHome',
    component:LogedHome

  },

]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
