// localStorageUtils.js

// Get value from local storage
export function getValue(key) {
    return localStorage.getItem(key);
  }
  
  // Set value in local storage
  export function setValue(key, value) {
    localStorage.setItem(key, value);
  }