import { createPinia,defineStore } from 'pinia'

export const useImageStore=defineStore({
    id:'image',
    state:()=>({
        images:[
            {
                src:'../images/japan.png',
                title:'Australia',
                content:'abs'
            },
            {
                src:'../images/japan.png',
                title:'Japan',
                content:'abs'
            },
            {
                src:'../images/greece.png',
                title:'Greece',
                content:'abs'
                
            },
            {
                src:'../images/zeeland.png',
                title:'zeeland',
                content:'abs'
            },
        ]
    })
})

// export const pinia = createPinia()
