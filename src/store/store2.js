import { createPinia,defineStore } from 'pinia'

export const useImageStore2=defineStore({
    id:'image',
    state:()=>({
        images:[
            {
                src:'../images/rec1.png',
                title:'Australia',
                content:'abs'
            },
            {
                src:'../images/rec2.png',
                title:'Japan',
                content:'abs'
            },
            {
                src:'../images/rec3.png',
                title:'Greece',
                content:'abs'
                
            },
            {
                src:'../images/rec4.png',
                title:'zeeland',
                content:'abs'
            },
        ]
    })
})

// export const pinia = createPinia()
