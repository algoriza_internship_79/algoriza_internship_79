import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import { createPinia } from 'pinia'
import './assets/tailwind.css'
import { pinia } from './store/store.js'

// createApp(App).use(router).mount('#app')


// app.use(pinia); 
const app = createApp(App);
// app.use(createPinia())

// const pinia =createPinia();
//  app.use(createPinia())

app.use(router);
app.mount('#app');