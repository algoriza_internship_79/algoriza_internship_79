/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./index.html",'./src/**/*.{vue,js,ts,jsx,tsx}'],
  theme: {
    extend: {
      colors:{
        "Blue":"#2F80ED",
        "Blue1": "#4796FF",
        "Blue2": "#2366BF",
        "Gray1":"#333333",
        "Gray2":"#4F4F4F",
        "Gray3":"#EBEBEB",
        "Gray4":"#BDBDBD",
        "Gray5":"#E0E0E0",
        "Gray6":"#F2F2F2",
        "Black1":"#181818",
        "Black2":"#1B1F2D",
        "bgColor":"#FFFFFF",
        "beigColor":"#FCEFCA",
        "Yellow1":"#F2C94C",
        "Red":"#EB5757",
        "popUp":"rgba(0,0,0,0.2)",
        "linearGradient":"#2969BF"
      }
    },
    fontFamily:{
      "mainFont": ['Work Sans', 'sans-serif'],
    },
    container:{
      padding:"1rem",
      center:true
    },
    screens:{
      sm:"640px",
      md:"768px"
    },
    // margin: {
    //   ng: "-30px",
    //   ng2: "-45px",
    //   ng3: "-70px",
    //   ng4: "-3px",
    //   ng5: "-183px",
    //   ng6: "-204px",
    //   ng7: "-435px",
    //   m15: "59px",
    //   m16: "264.5px",
    //   m17: "1174.8px",
    //   m18: "635px",
    //   m19: "477.8px",
    //   m20: "392.5px",
    // },
    dropShadow: {
      '3xl': '0 3px 8px rgba(0, 0, 0, 0.15)',
    }
  },
  plugins: [],
  purge: ['./src/**/*.{vue,js,ts,jsx,tsx}',
  ],
  darkMode: false,
  variants: {},

}